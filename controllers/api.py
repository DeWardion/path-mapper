# Here go your api methods.
    
def get_user():
    post_id=request.vars.post_id
    user = get_user_username()
    return response.json(dict(user=user))

def delete_post():
    post_id=request.vars.post_id
    db(db.post.id == post_id).delete()
    return "ok"
    
@auth.requires_signature()    
def edit_post():
    post_id=request.vars.post_id
    db(db.post.id == post_id).update(
        post_title=request.vars.post_title,
    )
    # We return the id of the new post, so we can insert it along all the others.
    return "ok"
    
@auth.requires_signature()
def add_post():
    post_id = db.post.insert(
        post_private = request.vars.post_privacy,
        post_title=request.vars.post_title,
        post_path=request.vars.post_path,
    )
    # We return the id of the new post, so we can insert it along all the others.
    return response.json(dict(post_id=post_id))


def get_post_list():
    results = []
    if auth.user is None:
        # Not logged in.
        rows = db(db.post.post_private == 'False' ).select(orderby=~db.post.post_time)
        for row in rows:
            # We get directly the list of all the users who liked the post. 
            likerows = db((db.thumb.post_id == row.id) & (db.thumb.thumb_state == 'u')).select(db.thumb.user_email)
            # If the user is logged in, we remove the user from the set.
            likers_set = set([r.user_email for r in likerows])
            if auth.user:
                likers_set -= {auth.user.email}
            likers_list = list(likers_set)
            likers_list.sort()
            # We get directly the list of all the users who liked the post. 
            dislikerows = db((db.thumb.post_id == row.id) & (db.thumb.thumb_state == 'd')).select(db.thumb.user_email)
            # If the user is logged in, we remove the user from the set.
            dislikers_set = set([r.user_email for r in dislikerows])
            if auth.user:
                dislikers_set -= {auth.user.email}
            dislikers_list = list(dislikers_set)
            dislikers_list.sort()
            results.append(dict(
                id=row.id,
                post_title=row.post_title,
                post_path=row.post_path,
                post_user=row.post_user,
                post_author=row.post_author,
                post_likes=likers_list,
                post_dislikes=dislikers_list,
                thumb = None,
            ))
    else:
        # Logged in.
        rows = db(db.post.post_private == 'False').select(db.post.ALL, db.thumb.ALL,
                            left=[
                                db.thumb.on((db.thumb.post_id == db.post.id) & (db.thumb.user_email == auth.user.email)),
                                
                            ],
                            orderby=~db.post.post_time)
        for row in rows:
            # We get directly the list of all the users who liked the post. 
            rows = db((db.thumb.post_id == row.post.id) & (db.thumb.thumb_state == 'u')).select(db.thumb.user_email)
            # If the user is logged in, we remove the user from the set.
            likers_set = set([r.user_email for r in rows])
            if auth.user:
                likers_set -= {auth.user.email}
            likers_list = list(likers_set)
            likers_list.sort()
            # We get directly the list of all the users who liked the post. 
            rows = db((db.thumb.post_id == row.post.id) & (db.thumb.thumb_state == 'd')).select(db.thumb.user_email)
            # If the user is logged in, we remove the user from the set.
            dislikers_set = set([r.user_email for r in rows])
            if auth.user:
                dislikers_set -= {auth.user.email}
            dislikers_list = list(dislikers_set)
            dislikers_list.sort()
            results.append(dict(
                id=row.post.id,
                post_title=row.post.post_title,
                post_path=row.post.post_path,
                post_user=row.post.post_user,
                post_author=row.post.post_author,
                post_likes=likers_list,
                post_dislikes=dislikers_list,
                thumb = None if row.thumb.id is None else row.thumb.thumb_state,
                like = True if row.thumb.thumb_state == 'u' else False,
                dislike = True if row.thumb.thumb_state == 'd' else False,
            ))
    # For homogeneity, we always return a dictionary.
    return response.json(dict(post_list=results))

def get_user_post_list():
    results = []
    # Logged in.
    if auth.user != None:
        user = get_user_username()
        rows = db(db.post.post_user == user).select(db.post.ALL, db.thumb.ALL,
                            left=[
                                db.thumb.on((db.thumb.post_id == db.post.id) & (db.thumb.user_email == auth.user.email)),
                                
                            ],
                            orderby=~db.post.post_time)
        
        for row in rows:
            # We get directly the list of all the users who liked the post. 
            rows = db((db.thumb.post_id == row.post.id) & (db.thumb.thumb_state == 'u')).select(db.thumb.user_email)
            # If the user is logged in, we remove the user from the set.
            likers_set = set([r.user_email for r in rows])
            if auth.user:
                likers_set -= {auth.user.email}
            likers_list = list(likers_set)
            likers_list.sort()
            # We get directly the list of all the users who liked the post. 
            rows = db((db.thumb.post_id == row.post.id) & (db.thumb.thumb_state == 'd')).select(db.thumb.user_email)
            # If the user is logged in, we remove the user from the set.
            dislikers_set = set([r.user_email for r in rows])
            if auth.user:
                dislikers_set -= {auth.user.email}
            dislikers_list = list(dislikers_set)
            dislikers_list.sort()
            results.append(dict(
                id=row.post.id,
                post_privacy=row.post.post_private,
                post_title=row.post.post_title,
                post_path=row.post.post_path,
                post_user=row.post.post_user,
                post_author=row.post.post_author,
                post_likes=likers_list,
                post_dislikes=dislikers_list,
                thumb = None if row.thumb.id is None else row.thumb.thumb_state,
                like = True if row.thumb.thumb_state == 'u' else False,
                dislike = True if row.thumb.thumb_state == 'd' else False,
            ))
        # For homogeneity, we always return a dictionary.
    return response.json(dict(post_list=results))
    
@auth.requires_signature()
def set_like():
    post_id = int(request.vars.post_id)
    like_status = request.vars.like.lower().startswith('t');
    if like_status:
        db.thumb.update_or_insert(
            (db.thumb.post_id == post_id) & (db.thumb.user_email == auth.user.email),
            post_id = post_id,
            user_email = auth.user.email,
            thumb_state = 'u'
        )
    else:
        db((db.thumb.thumb_state == 'u') & (db.thumb.post_id == post_id) & (db.thumb.user_email == auth.user.email)).delete()
    return "ok" # Might be useful in debugging.

def get_likers():
    """Gets the list of people who liked a post."""
    post_id = int(request.vars.post_id)
    # We get directly the list of all the users who liked the post. 
    rows = db((db.thumb.post_id == post_id) & (db.thumb.thumb_state == 'u')).select(db.thumb.user_email)
    # If the user is logged in, we remove the user from the set.
    likers_set = set([r.user_email for r in rows])
    if auth.user:
        likers_set -= {auth.user.email}
    likers_list = list(likers_set)
    likers_list.sort()
    # We return this list as a dictionary field, to be consistent with all other calls.
    return response.json(dict(likers=likers_list))

@auth.requires_signature()
def set_dislike():
    post_id = int(request.vars.post_id)
    dislike_status = request.vars.dislike.lower().startswith('t');
    if dislike_status:
        db.thumb.update_or_insert(
            (db.thumb.post_id == post_id) & (db.thumb.user_email == auth.user.email),
            post_id = post_id,
            user_email = auth.user.email,
            thumb_state = 'd'
        )
    else:
        db((db.thumb.thumb_state == 'd') & (db.thumb.post_id == post_id) & (db.thumb.user_email == auth.user.email)).delete()
    return "ok" # Might be useful in debugging.

def get_dislikers():
    """Gets the list of people who liked a post."""
    post_id = int(request.vars.post_id)
    # We get directly the list of all the users who liked the post. 
    rows = db((db.thumb.post_id == post_id) & (db.thumb.thumb_state == 'd')).select(db.thumb.user_email)
    # If the user is logged in, we remove the user from the set.
    dislikers_set = set([r.user_email for r in rows])
    if auth.user:
        dislikers_set -= {auth.user.email}
    dislikers_list = list(dislikers_set)
    dislikers_list.sort()
    # We return this list as a dictionary field, to be consistent with all other calls.
    return response.json(dict(dislikers=dislikers_list))

def make_public():
    post_id=request.vars.post_id
    db(db.post.id == post_id).update(
        post_private = 'False',
    )
    return "ok"

def make_private():
    post_id=request.vars.post_id
    db(db.post.id == post_id).update(
        post_private = 'True',
    )
    return "ok"
