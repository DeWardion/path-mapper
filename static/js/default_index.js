// This is the js for the default/index.html view.

//var is_logged_in = {{='false' if auth.user is None else 'true'}};
var app = function() {
    var self = {};
    var track;
    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };
    
    self.get_user = function(){
        $.getJSON(get_user_url, function(data){
            self.vue.user = data.user;
        });
    }
    
    self.get_posts = function() {
        $.getJSON(get_post_list_url,
            function(data) {
                // I am assuming here that the server gives me a nice list
                // of posts, all ready for display.
                self.vue.post_list = data.post_list;
                console.log(self.vue.post_list);
                //console.log(self.vue.post_list);
                // Post-processing.
                self.process_posts();
            }
        );
        
        //console.log("I fired the get");
    };
    
    self.get_user_posts = function(){
        $.getJSON(get_user_post_list_url,
            function(data) {
                // I am assuming here that the server gives me a nice list
                // of posts, all ready for display.
                self.vue.post_list = data.post_list;
                console.log(self.vue.post_list);
                //console.log(self.vue.post_list);
                // Post-processing.
                self.process_posts();
            }
        );
    }
    
    // Enumerates an array.
    var enumerate = function(v) { var k=0; return v.map(function(e) {e._idx = k++;});};
    
    self.process_posts = function() {
        // This function is used to post-process posts, after the list has been modified
        // or after we have gotten new posts. 
        // We add the _idx attribute to the posts. 
        enumerate(self.vue.post_list);
        // We initialize the smile status to match the like. 

        self.vue.post_list.map(function (e) {
            // I need to use Vue.set here, because I am adding a new watched attribute
            // to an object.  See https://vuejs.org/v2/guide/list.html#Object-Change-Detection-Caveats
            // The code below is commented out, as we don't have smiles any more. 
            // Replace it with the appropriate code for thumbs. 
            // // Did I like it? 
            // If I do e._smile = e.like, then Vue won't see the changes to e._smile . 
            Vue.set(e, '_editpost', false);
            Vue.set(e, '_like', e.like); 
            // Who liked it?
            Vue.set(e, '_likers', []);
            // Do I know who liked it? (This could also be a timestamp to limit refresh)
            Vue.set(e, '_likers_known', false);
            // Do I show who liked? 
            Vue.set(e, '_dislike', e.dislike);
            Vue.set(e, '_dislikers',[]);
            Vue.set(e, '_dislikers_known', false);
            //Vue.set(e, '_count', self.get_count(e._idx));
            Vue.set(e, '_count_known', false);
            Vue.set(e, '_thumb_state', e.thumb);
            Vue.set(e, '_path', []);
        });
        //console.log("HIconsole");
    };
    
    self.submit_drawn = function(){
        var path = get_drawn();
        console.log(path);
        if (path.length > 1){
            var jsonObj = toObject(path);
            formatted = JSON.stringify(jsonObj);
            self.vue.form_path = formatted;
            draw = [];
            self.vue.drawn = true;
        } else {
            alert("Please add at least two points");
        }
    }
    
    self.upload_json=function(){
        //code from
        //https://stackoverflow.com/questions/36127648/uploading-a-json-file-and-using-it
        //https://jsfiddle.net/Ln37kqc0/752/
        var files = document.getElementById('upload_file').files;
        if (files.length <= 0) {
            alert("please choose a JSON file");
            return false;
        }
        var formatted;
        var reader = new FileReader();
        reader.onload = function(event){
            var jsonObj = JSON.parse(event.target.result);
            formatted = JSON.stringify(jsonObj);
            self.vue.form_path = formatted;
            show_upload_data(jsonObj);
            self.vue.uploaded = true;
            //console.log(self.vue.form_path);
            //console.log("I am best");
        }
        reader.readAsText(files.item(0));
    }
    
    self.preview_path=function(){
        var path = [];
        var files = document.getElementById('upload_file').files;
        if (files.length <= 0) {
            alert("please choose a JSON file");
            return false;
        }
        var formatted;
        var reader = new FileReader();
        reader.onload = function(event){
            var jsonObj = JSON.parse(event.target.result);
            formatted = JSON.stringify(jsonObj);
            //self.vue.form_path = formatted;
            show_upload_data(jsonObj);
            for(key in jsonObj){
                path.push([jsonObj[key].lat, jsonObj[key].lng]);
            }
            draw_map(path);
            //console.log(self.vue.form_path);
            //console.log("I am best");
        }
        reader.readAsText(files.item(0));
    }
    
    self.show_format = function(){
        var format_item = document.getElementById("format");
        var format = 'Format:<br>{ <br> "0":{ <br> "lat":37.000132, <br> "lng":-122.054404, <br> }, <br> "1": { <br> "lat":36.999989, <br> "lng":-122.054399, <br> } <br> }';
        var formatted = JSON.stringify(format);
        format_item.innerHTML = format;
    }
    
    self.change_edit_form = function(post_idx){
        console.log("hi");
        var p = self.vue.post_list[post_idx];
        p._editpost = !p._editpost;
    }
    
    self.edit_post = function (post_idx) {
        var p = self.vue.post_list[post_idx];
        self.change_edit_form(post_idx);
    }
    
    self.edit_post_post = function (post_idx) {
        var p = self.vue.post_list[post_idx];
        self.change_edit_form(post_idx);
        $.web2py.disableElement($("#edit-post"));
        var sent_title = p.post_title; // Makes a copy 
        $.post(edit_post_url,
            // Data we are sending.
            {
                post_id:p.id,
                post_title: p.post_title,
            });
        $.web2py.enableElement($("#edit-post"));
    }
    
    self.delete_post = function(post_idx){
        var p = self.vue.post_list[post_idx];
        console.log(post_idx);
        console.log("trying to delete");
        $.post(delete_post_url,
            // Data we are sending.
            {
                post_id:p.id,
            },
            function (data) {
                //post_list[post_idx].pop;
                var new_post_list = [];
                for(post_idx2 in self.vue.post_list){
                    new_post_list.push(self.vue.post_list[post_idx2]);
                    if(post_idx2 == post_idx){
                        new_post_list.pop();
                    }
                }
                self.get_user_posts();
            });
    }
    
    self.add_post=function(){
        $.web2py.disableElement($("#add-post"));
        var sent_title = self.vue.form_title; // Makes a copy 
        //console.log(self.vue.form_path);
        var sent_content = self.vue.form_path; // 
        var curr_user = self.vue.user;
        var privacy_value = self.vue.privacy_setting
        console.log(privacy_value);
        if(self.vue.form_path == "" || self.vue.form_title == ""){
            if(self.vue.form_title == ""){
                alert("Please Type a Title");
            }
            if(self.vue.form_path == ""){
                alert("No Path Found");
            }
            $.web2py.enableElement($("#add-post"));
        } else {
            $.post(add_post_url,
                // Data we are sending.
                {
                    post_privacy: self.vue.privacy_setting,
                    post_title: self.vue.form_title,
                    post_path: self.vue.form_path,
                },
            // What do we do when the post succeeds?
            function (data) {
                // Re-enable the button.
                $.web2py.enableElement($("#add-post"));
                // Clears the form.
                self.vue.form_title = "";
                self.vue.form_path = "";
                self.vue.form_file = "";
                // Adds the post to the list of posts.
                var new_post = {
                    post_user: curr_user,
                    id: data.post_id,
                    post_title: sent_title,
                    post_path: sent_content,
                    post_likes: [],
                    post_dislikes: [],
                };
                //I know, sorry. I'm not supposed to call the server
                //just to show posts, but I could not make up of a way
                //to show the correct "make public" or "make private"
                //when initially adding a post
                if(self.vue.posts_type == "all"){
                    self.vue.user_posts = false;
                    self.get_posts();
                    //self.get_user_posts();
                } else if (self.vue.posts_type == "mine"){
                    self.vue.user_posts = true;
                    self.get_user_posts();
                }
                self.vue.cancel_post();
            });
        }
    }
    
    self.show_path = function(post_idx){
        //console.log(self.vue.post_list);
        //console.log(post_idx);
        var p = self.vue.post_list[post_idx];
        //console.log(p);
        
        var path = p.post_path;
        var jsonObj = JSON.parse(path);
        show_upload_data(jsonObj);
        p._path = [];
        console.log(p._path);
        for(key in jsonObj){
            p._path.push([jsonObj[key].lat, jsonObj[key].lng]);
        }
        draw_map(p._path);
    }
    
    self.like_mouseover = function (post_idx) {
        // When we mouse over something, the face has to assume the opposite
        // of the current state, to indicate the effect.
        var p = self.vue.post_list[post_idx];
        p._like = !p.like;
    };

    self.like_click = function (post_idx) {
        // The like status is toggled; the UI is not changed.
        var p = self.vue.post_list[post_idx];
        if(p.like){
            p._count = p._count -1;
        } else {
            p._count = p._count + 1;
            if(p.dislike){
                p._count = p._count + 1;
            }
            
        }
        p._dislike = false;
        p.dislike = false;
        //console.log(p.like);
        p.like = !p.like;
        p._like = !p._like;
        // We need to post back the change to the server.
        $.post(set_like_url, {
            post_id: p.id,
            like: p.like
        }); // Nothing to do upon completion.
    };

    self.like_mouseout = function (post_idx) {
        // The like and smile status coincide again.
        var p = self.vue.post_list[post_idx];
        p._like = p.like;
        //console.log("like mouse out");
    };
    
    self.dislike_mouseover = function (post_idx) {
        // When we mouse over something, the face has to assume the opposite
        // of the current state, to indicate the effect.
        var p = self.vue.post_list[post_idx];
        p._dislike = !p.dislike;
    };

    self.dislike_click = function(post_idx) {
        // The like status is toggled; the UI is not changed.
        var p = self.vue.post_list[post_idx];
        if(p.dislike){
            p._count = p._count + 1;
        } else {
            p._count = p._count - 1;
            if(p.like){
                p._count = p._count - 1;
            }
        }
        p.like = false;
        p._like = false;
        p.dislike = !p.dislike;
        p._dislike = !p._dislike;
        // We need to post back the change to the server.
        $.post(set_dislike_url, {
            post_id: p.id,
            dislike: p.dislike
        }); // Nothing to do upon completion.
    };

    self.dislike_mouseout = function(post_idx) {
        // The like and smile status coincide again.
        var p = self.vue.post_list[post_idx];
        p._dislike = p.dislike;
    };
    
    self.get_count = function (post_idx){
        var p = self.vue.post_list[post_idx];
        /*if (!p._dislikers_known) {
            $.getJSON(get_dislikers_url, {post_id: p.id}, function (data) {
                p._dislikers = data.dislikers
            })
            p._dislikers_known = true;
        }
        
        if (!p._likers_known) {
            $.getJSON(get_likers_url, {post_id: p.id}, function (data) {
                p._likers = data.likers
            })
            p._likers_known = true;
        }*/
        //console.log(p._likers_known);
        //console.log(p._dislikers_known);
        p._count = p.post_likes.length - p.post_dislikes.length;
        //console.log(p._count);
        //console.log(p._likers.length);
        //console.log(p._dislikers.length);
        if(p.dislike){
            p._count = p._count - 1;
            //console.log("HI")
        }
        if(p.like){
            p._count = p._count + 1;
            //console.log("HI")
        }
        return p._count;
    }
    
    self.change_to_upload = function (){
        console.log(self.vue.upload);
        self.vue.options =  true;
        self.vue.upload = true;
    }
    
    self.change_to_track = function(){
        self.vue.options =  true;
        self.vue.track = true;
        console.log("hi");
    }
    
    self.change_to_draw = function(){
        self.vue.options =  true;
        self.vue.draw = true;
        clearMap();
    }
    
    self.track_start = function(){
        self.vue.tracking = true;
        self.vue.track_paused = false;
        map_track();
        track = setInterval(function(){ map_track() }, 100);
    }
    
    self.track_pause = function(){
        self.vue.track_paused = true;
        clearInterval(track);
        console.log(self.vue.track_paused);
    }
    
    self.finish_tracking = function(){

        var path = get_drawn();
        console.log(path);
        if (path.length > 1){
            var jsonObj = toObject(path);
            formatted = JSON.stringify(jsonObj);
            self.vue.form_path = formatted;
            draw = [];
            self.vue.drawn = true;
            self.vue.tracked = true;
            clearInterval(track);
        } else {
            alert("Please Track a little longer");
        }
    }
    
    self.cancel_post = function(){
        self.vue.show_form = !self.vue.show_form;
        self.vue.cancel_options();
        fullClearMap();
    }
    
    self.cancel_options = function(){
        self.vue.options =  false;
        self.vue.upload = false;
        self.vue.draw = false;
        self.vue.track = false;
        self.vue.tracked = false;
        self.vue.form_path="";
        draw = [];
        clearMap();
        var time_item = document.getElementById("time");
        time_item.innerHTML = "";
        var dist_item = document.getElementById("length");
        dist_item.innerHTML = "";
        var format_item = document.getElementById("format");
        format_item.innerHTML = "";
        self.vue.uploaded = false;
        self.vue.tracking = false;
        self.vue.drawn = false;
        clearInterval(track);
    }
    self.change_posts = function(){
        if(self.vue.posts_type == "all"){
            self.vue.user_posts = false;
            self.get_posts();
            //self.get_user_posts();
        } else if (self.vue.posts_type == "mine"){
            self.vue.user_posts = true;
            self.get_user_posts();
        }
    }
    self.make_private = function(post_idx){
        var p = self.vue.post_list[post_idx];
        $.post(make_private_url, {
            post_id: p.id,
        }, function() {
            if(self.vue.posts_type == "all"){
                self.vue.user_posts = false;
                self.get_posts();
                //self.get_user_posts();
            } else if (self.vue.posts_type == "mine"){
                self.vue.user_posts = true;
                self.get_user_posts();
            }
        }); // Nothing to do upon completion.
    }
    self.make_public = function(post_idx){
        var p = self.vue.post_list[post_idx];
        $.post(make_public_url, {
            post_id: p.id,
        },function(){
            if(self.vue.posts_type == "all"){
                self.vue.user_posts = false;
                self.get_posts();
                //self.get_user_posts();
            } else if (self.vue.posts_type == "mine"){
                self.vue.user_posts = true;
                self.get_user_posts();
            }
        }); // Nothing to do upon completion.
    }
    
    self.download_path = function(post_idx){
    }
    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            form_title: "",
            form_path: "",
            form_file: "",
            post_list: [],
            user_post_list: [],
            polylines: [],
            show_form: false,
            user_known: false,
            user: "",
            private: "",
            upload: false,
            track: false,
            draw: false,
            options: false,
            uploaded: false,
            tracking: false,
            drawn: false,
            track_paused: false,
            tracked: false,
            privacy_setting: "False",
            user_posts: false,
            posts_type: "all",
            posts_sort: "new",
        },
        methods: {
            get_user: self.get_user,
            //post stuff
            cancel_options: self.cancel_options,
            change_posts: self.change_posts,
            make_private: self.make_private,
            make_public: self.make_public,
            get_posts: self.get_posts,
            get_user_posts: self.get_user_posts,
            //track stuff
            change_to_track: self.change_to_track,
            track_start: self.track_start,
            track_pause: self.track_pause,
            finish_tracking: self.finish_tracking,
            
            //drawn stuff
            change_to_draw: self.change_to_draw,
            submit_drawn: self.submit_drawn,
            
            
            //upload stuff
            upload_json: self.upload_json,
            change_to_upload: self.change_to_upload,
            preview_path: self.preview_path,
            show_format: self.show_format,
            
            //download stuff
            download_path: self.download_path,
            //post stuff
            cancel_post: self.cancel_post,
            add_post: self.add_post,
            show_path: self.show_path,
            delete_post: self.delete_post,
            
            // Likers. 
            like_mouseover: self.like_mouseover,
            like_mouseout: self.like_mouseout,
            like_click: self.like_click,
            
            //Dislikers
            dislike_mouseover: self.dislike_mouseover,
            dislike_mouseout: self.dislike_mouseout,
            dislike_click: self.dislike_click,
            
            // Show count
            get_count: self.get_count,
            
            //edit post
            editpost: self.editpost,
            change_edit_form: self.change_edit_form,
            edit_post: self.edit_post,
            edit_post_post: self.edit_post_post,
            
        }
    });

    /*if (is_logged_in) {
        $("#add_post").show();
    }*/
    self.get_user();
    self.get_posts();
    //self.get_user_posts();
    return self;
};



var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});
