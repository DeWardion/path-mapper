var mymap = new L.map('mapid').setView([36.995698, -122.059135], 15);
L.tileLayer('https://api.mapbox.com/styles/v1/deward/cjctqjzpn1bns2so0l1liwkpq/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
    maxZoom: 18,
    minZoom: 15,
    accessToken: 'pk.eyJ1IjoiZGV3YXJkIiwiYSI6ImNqY3NoanlzaDBlcnAycXFxYTFhdGl2dm4ifQ.I-o2qW918K0PzZ6W1nqWxQ'

}).addTo(mymap);

mymap.removeControl(mymap.zoomControl);
mymap.locate({setView: true, maxZoom: 20});
var polylines = [];
var draw = [];
var circle;
var trackcount = 0;
var track = [];
var draw_map = function(latlng){
    clearMap();
    var polyline = L.polyline(latlng);
    polylines.push(polyline);
    if(latlng.length > 0){
        mymap.fitBounds(polyline.getBounds(), {padding: [50,50]});
    }
    mymap.addLayer(polyline);
}

var draw_position = function(latlng){
    clearMap();
    console.log("hi");
    var polyline = L.polyline(latlng);
     console.log("hi2");
    polylines.push(polyline);
    
    mymap.setView(latlng[latlng.length-1], 20)
    mymap.addLayer(polyline);
}
var distance = function(node1, node2){
    var x1 = node1[0];
    var x2 = node2[0];
    var y1 = node1[1];
    var y2 = node2[1];
    
    //convert distance between axis into radians
    var dLat = (x2-x1) * Math.PI / 180;
    var dLon = (y2-y1) * Math.PI / 180;

    //conver lat into radians
    var lat1 = x1 * Math.PI / 180;
    var lat2 = x2 * Math.PI / 180;

    //Haversine forumla and convert into miles
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    dist = 3958 * c ;
    console.log(dist);
    return dist;
}

var show_upload_data = function(jsonObj){
    var path = [];
    var dist = 0;
    var constSpeed = 4;
    for(key in jsonObj){
        path.push([jsonObj[key].lat, jsonObj[key].lng]);
    }
    for(var i = 1; i < path.length; i++){
        dist += distance(path[i],path[i-1]);
    }
    var show_dist = dist.toFixed(2);
    var dist_item = document.getElementById("length");
    dist_item.innerHTML = "Distance: " + show_dist + " Miles";
    
    var estTime = Math.ceil(dist/constSpeed*60);
    var time_item = document.getElementById("time");
    console.log(estTime);
    time_item.innerHTML = "Estimated Time: " + estTime + " minute(s)";
    
}
var clearMap = function(){
    for(var p in polylines){
        if(polylines[p] != undefined){
            mymap.removeLayer(polylines[p]);
        }
    }
    for(var d in draw){
        if(draw[d] != undefined){
            mymap.removeLayer(draw[d]);
        }
    }
    if (circle != undefined) {
      mymap.removeLayer(circle);
    }
    //mymap.closePopup();
}

var fullClearMap = function(){
    for(var p in polylines){
        if(polylines[p] != undefined){
            mymap.removeLayer(polylines[p]);
        }
    }
    for(var d in draw){
        if(draw[d] != undefined){
            mymap.removeLayer(draw[d]);
        }
    }
    if (circle != undefined) {
      mymap.removeLayer(circle);
    }
    mymap.closePopup();
}

var popup = L.popup();
mymap.on('click', onMapClick);
function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(mymap);
    //console.log(e.latlng);
}

var add_point = function(){

    if (popup._latlng != null){
        draw.push(popup._latlng);
        popup
            .setLatLng(draw[draw.length -1])
            .setContent("Point " + draw[draw.length -1].toString() + " added")
            .openOn(mymap);
        draw_map(draw);
    } else {
        alert("Please click on the map to add a point");
    }
}

var remove_point = function(){
    popup
        .setLatLng(draw[draw.length -1])
        .setContent("Point " + draw[draw.length -1].toString() + " removed")
        .openOn(mymap);
    draw.pop();
    draw_map(draw);
}

var get_drawn = function(){
    return draw;
}

//code from https://stackoverflow.com/questions/4215737/convert-array-to-object
function toObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
        if (arr[i] !== undefined) rv[i] = arr[i];
    return rv;
}

function onLocationFound(e) {
    var radius = 20;
    var avg_lat;
    var avg_lng;
    var total_lat;
    var total_lng;
    var avglatlng = [];
    if (track.length > 4){
        clearMap();
        var arr=[];
        trackcount = 0;
        total_lng = 0;
        total_lat = 0;
        for(latlngs in track){
            console.log(track);
            total_lng = total_lng + track[latlngs].lng;
            total_lat = total_lat + track[latlngs].lat;
            console.log(total_lng);
            console.log(total_lat);
        }
        avg_lat = total_lat/5;
        avg_lng = total_lng/5;
        avglatlng.push(avg_lat);
        avglatlng.push(avg_lng);
        e.latlng.lat =avg_lat;
        e.latlng.lng =avg_lng;
        draw.push(e.latlng);
        draw_position(draw);
        circle = L.circle(e.latlng, radius).addTo(mymap);
        console.log("HI");
        track = [];
    } else {
        trackcount = trackcount + 1;
        track.push(e.latlng);
    }
}

var map_track = function(){
    mymap.locate({enableHighAccuracy: true });
    mymap.on('locationfound', onLocationFound);
}
