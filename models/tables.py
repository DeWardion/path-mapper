# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.




# after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

import datetime

def get_user_email():
    return None if auth.user is None else auth.user.email

def get_user_username():
    return None if auth.user is None else auth.user.username
    
def get_current_time():
    return datetime.datetime.utcnow()
    

db.define_table('post',
                Field('post_author', default=get_user_email()),
                Field('post_user', default=get_user_username()),
                Field('post_title','text', default="No Title"),
                Field('post_path', 'text'),
                Field('post_time', 'datetime', default=get_current_time()),
                Field('post_private', 'boolean', default=False),
                )

db.define_table('thumb',
                Field('user_email'), # The user who thumbed, easier to just write the email here.
                Field('post_user', default=get_user_username()),
                Field('post_id', 'reference post'), # The thumbed post
                Field('thumb_state'), # This can be 'u' for up or 'd' for down, or None for... None.
                )
